import AxiosInterceptor from '../api/interceptor';
const axios = new AxiosInterceptor();

export default {
  login(phone){
    return axios.post("/app/login",{
      'phone': phone
    });
  },
  verifySMS(otp, phone){
    return axios.post("/app/verifySms",{
      'otp': otp,
      'phone': phone
    });
  },
  getProfile(){
    return axios.get("/app/accounts");
  },
  getAccountDetailByCat(id){
    return axios.get("/accounts/level-detail/"+id);
  },

  getExamHistory(page) {
    return axios.get("/accounts/exam-history?page="+page);
  },
  getAccountLevels() {
    return axios.get("/accounts/level-detail");
  },
  getCategoryList() {
    return axios.get("/question-categories?sort=asc");
  },
  getRankList() {
    return axios.get("/accounts/get-rank/");
  },
  getCategory(id) {
    return axios.get("/question-categories/" + id);
  },
  getAgency(id) {
    return axios.get("/question-categories/" + id);
  },

  getExamQuestion(category_id, agency_id) {
    return axios.get("/exams/get?category_id="+category_id+"&agency_id="+agency_id);
  },
  getExamConfig(agency_id) {
    return axios.get("/agency-level-configs/by-agency/"+agency_id);
  },
  checkExam(answers) {
    return axios.post("/exams/check", answers);
  },
};