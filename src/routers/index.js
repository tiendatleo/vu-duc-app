import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);

import Login from '../pages/auth/login'
import Otp from '../pages/auth/otp'
import Home from '../pages/home'
import Exam from '../pages/exam'
import ExamResult from '../pages/exam-result'


export default new VueRouter({
    // base: '/admin', // mode history only
    mode: "hash",
    linkActiveClass: "is-active",
    routes: [
        {
            name: "login",
            path: "/login",
            meta: {
                title: 'Đăng nhập - Học viện Vũ Đức',
                metaTags: [
                    {
                        name: 'description',
                        content: 'The home page of our example app.'
                    },
                    {
                        property: 'og:description',
                        content: 'The home page of our example app.'
                    }
                ],
                public: true, auth: false
            },
            component: Login
        },
        {
            name: "otp",
            path: "/otp",
            meta: {
                title: 'Nhập mã OTP',
                metaTags: [
                    {
                        name: 'description',
                        content: 'The home page of our example app.'
                    },
                    {
                        property: 'og:description',
                        content: 'The home page of our example app.'
                    }
                ],
                public: true, auth: false
            },
            component: Otp
        },
        {
            name: "home",
            path: "/",
            meta: {
                title: 'Học viện Vũ Đức',
                metaTags: [
                    {
                        name: 'description',
                        content: 'The home page of our example app.'
                    },
                    {
                        property: 'og:description',
                        content: 'The home page of our example app.'
                    }
                ],
                public: true, auth: false
            },
            component: Home
        },
        {
            name: "exam",
            path: "/exam/:category_id/:agency_id",
            meta: {
                title: 'Trắc nghiệm Vũ Đức',
                metaTags: [
                    {
                        name: 'description',
                        content: 'The home page of our example app.'
                    },
                    {
                        property: 'og:description',
                        content: 'The home page of our example app.'
                    }
                ],
                public: true, auth: false },
            component: Exam
        },
        {
            name: "examResult",
            path: "/exam-result/:category_id/:agency_id/:is_pass/:correct/:min/:total",
            meta: {
                title: 'Kết quả thi',
                metaTags: [
                    {
                        name: 'description',
                        content: 'The home page of our example app.'
                    },
                    {
                        property: 'og:description',
                        content: 'The home page of our example app.'
                    }
                ],
                public: true, auth: false },
            component: ExamResult
        },
    ]
});